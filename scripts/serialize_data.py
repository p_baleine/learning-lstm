#!/usr/bin/env python

import click
import pickle
import re

def to_vocab_map(input):
    s = set().union(*{c for line in input for c in line})
    return dict((c, i) for i, c in enumerate(sorted(list(s))))

def encode(input, vocab_map):
    return [vocab_map[c] for line in input for c in line]

@click.command()
@click.argument('input')
@click.option('--vocab', default='vocab.pickle', help='vocab file output')
@click.option('--data', default='data.pickle', help='data file output')
def serialize(input, vocab, data):
    """テキストファイルを引数にとり、辞書と辞書の値でインデックス
    されたデータをシリアライズして保存する
    """
    input_in = open(input, 'r')
    vocab_out = open(vocab, 'wb')
    data_out = open(data, 'wb')

    with input_in, vocab_out, data_out:
        vocab_map = to_vocab_map(input_in)
        input_in.seek(0)
        encoded = encode(input_in, vocab_map)

        pickle.dump(vocab_map, vocab_out)
        pickle.dump(encoded, data_out)

if __name__ == '__main__':
    serialize()
