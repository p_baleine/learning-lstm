import numpy as np
from functions import sigmoid

class LSTMCell():
    """LSTM cell(s)"""

    def __init__(self, input_size, hidden_size, num_layer):
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layer = num_layer

    def forward(self, W, x, prev_c, prev_h):
        indices = parameter_indices(self.input_size, self.hidden_size, self.num_layer)

        # forwardする間覚えておきたい隠れ層の格納先
        hs = []

        # bias分を加えておく
        prev_h = add_bias_ones(prev_h)

        for l in range(0, self.num_layer):
            if l > 0: x = add_bias_ones(hs[l - 1]) # 2層目移行は一つ前の層のh

            W_i2h = W[indices[(l * 2)]:indices[(l * 2) + 1]]
            W_h2h = W[indices[(l * 2) + 1]:indices[(l * 2) + 2]]

            i2h = x.dot(W_i2h)
            h2h = prev_h.dot(W_h2h)
            i2h_h2h = i2h + h2h

            hs = self.hidden_size

            n1 = i2h_h2h[hs * 0:hs * 1]
            n2 = i2h_h2h[hs * 1:hs * 2]
            n3 = i2h_h2h[hs * 2:hs * 3]
            n4 = i2h_h2h[hs * 3:hs * 4]

            in_gate = sigmoid(n1)
            forget_gate = sigmoid(n2)
            out_gate = sigmoid(n3)
            in_transform = np.tanh(n4)

            c = forget_gate * prev_c + in_gate * in_transform
            h = out_gate * np.tanh(c)
            # FIXME cとhをselfに覚えとく？

            hs.append(h)

        return c, h # このセルの状態と、出力

    def backward():
        # TODO 明日やる
        pass

class LSTM():
    """LSTMネットワーク"""

    def __init__(self, seq_length, input_size, hidden_size, num_layer):
        """ パラメタを初期化する

        引数
          seq_length: LSTMセルの数
          input_size: 入力のサイズ
          hidden_size: 隠れ層のサイズ
          num_layer: 隠れ層の数
        """

        self.W = np.random.randn(
            # 隠れ層1層目、入力のbias + 入力のサイズ + 隠れ層のbias + 隠れ層のサイズ
            ((1 + input_size + 1 + hidden_size)
             # 隠れ層2層目以降、(隠れ層のbias + 隠れ層のサイズ)が2つづつ
             + (num_layer - 1) * (1 + hidden_size) * 2),

            # input、forget、output、in-transformで4つ分
            4 * hidden_size
            # [- 1 / n^(0.5), 1 / n^(0.5)]で初期化
        ) / np.sqrt(input_size + hidden_size)

        # 入力のbiasと隠れ層のbiasをゼロ初期化
        self.W[parameter_indices(input_size, hidden_size, num_layer)[:-1]] = 0

        # 出力用のパラメーター
        self.W_o = np.random.randn(1 + hidden_size, input_size)

        self.seq_length = seq_length
        self.input_size = input_size
        self.hidden_size = hidden_size
        self.num_layer = num_layer

    def forward(self, X, c0, h0):
        # Xは(seq_length, input_size)型
        _seq_length, _input_size = X.shape
        assert (self.seq_length == _seq_length and
                self.input_size == _input_size), 'X must be(seq_length, input_size)'

        # bias向けのカラム追加
        X = add_bias_ones(X)

        # sequence分のセル
        cells = [LSTMCell(self.input_size, self.hidden_size, self.num_layer)
                 for _ in range(self.seq_length)]

        # sequenceを一周する間保持しておきたい値の格納先
        # 隠れ層用
        Hout = np.zeros((self.seq_length, self.hidden_size))
        # cell用
        C = np.zeros((self.seq_length, self.hidden_size))

        for t, cell in enumerate(cells):
            prev_c = C[t - 1] if t > 0 else c0
            prev_h = Hout[t - 1] if t > 0 else h0
            next_c, next_h = cell.forward(self.W, X[t], prev_c, prev_h)
            C[t] = next_c
            Hout[t] = next_h

        # デコード
        Out = add_bias_ones(Hout).dot(self.W_o)

        return Out, next_c, next_h

    def backward():
        # TODO
        pass

def add_bias_ones(X):
    """bias向けに1のカラムを追加した`X`を返す"""
    size = X.shape
    if len(size) == 1:
        _X = np.zeros((size[0] + 1))
        _X[1:] = X
        _X[0] = 1
    elif len(size) == 2:
        _X = np.zeros((size[0], size[1] + 1))
        _X[:, 1:] = X
        _X[:, 0] = 1
    return _X

def parameter_indices(input_size, hidden_size, num_layer):
    """input、forget、output、in-transformのindexのリストを返す"""
    first = [0, input_size + 1]
    rest = [b + (hidden_size + 1) * (l + 1) for l, b in
            enumerate([input_size + 1] * (2 * (num_layer - 1) + 1))]
    return first + rest
