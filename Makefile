INPUT_URL = https://github.com/karpathy/char-rnn/raw/master/data/tinyshakespeare/input.txt
INPUT_FILE = data/input.txt
VOCAB_FILE = data/vocab.pickle
TENSOR_FILE = data/data.pickle

$(VOCAB_DILE) $(TENSOR_FILE): $(INPUT_FILE)
	scripts/serialize_data.py $(INPUT_FILE) --vocab $(VOCAB_FILE) --data $(TENSOR_FILE)

$(INPUT_FILE):
	curl -L -o $(INPUT_FILE) $(INPUT_URL)

clean:
	rm -f $(INPUT_FILE) $(VOCAB_FILE) $(TENSOR_FILE)
