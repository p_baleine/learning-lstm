import numpy as np
from lstm import LSTM
from functions import softmax, one_hot
import pickle
import time

def chunk_index(l, n):
    for i in range(0, len(l), n):
        yield (i, i + n)

seq_length = 50
hidden_size = 128
num_layer = 2
epochs = 1

vocab_file = open('./data/vocab.pickle', 'rb')
data_file = open('./data/data.pickle', 'rb')

with vocab_file, data_file:
    vocab = pickle.load(vocab_file)
    data = pickle.load(data_file)

input_size = len(vocab)

data = data[:1001]

X = one_hot(np.array(data[:-1]), input_size)
Y = one_hot(np.array(data[1:]), input_size)

lstm = LSTM(seq_length, input_size, hidden_size, num_layer)
c = np.zeros(hidden_size)
h = np.zeros(hidden_size)

for e in range(epochs):
    epoch_start = time.clock()
    for i, j in chunk_index(X, seq_length):
        O, c, h = lstm.forward(X[i:j], c, h)
        O = softmax(O)
        loss = - 1 * np.sum(Y[i:j] * np.log(O)) / (j - i)
        print(loss)
    elapsed = (time.clock() - epoch_start) * 1000
    print('%d epoch, loss = %.6f (%.2f ms)' % (e, loss, elapsed))
