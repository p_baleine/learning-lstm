import numpy as np
import unittest
from lstm import add_bias_ones, parameter_indices

class TestLSTM(unittest.TestCase):
    def test_parameter_indices(self):
        indices = parameter_indices(20, 50, 3)
        self.assertEqual(indices[0], 0)
        self.assertEqual(indices[1], 21)
        self.assertEqual(indices[2], 72)
        self.assertEqual(indices[3], 123)
        self.assertEqual(indices[4], 174)
        self.assertEqual(indices[5], 225)
        self.assertEqual(indices[6], 276)

    def test_add_bias_ones(self):
        self.assertTrue(np.array_equal(add_bias_ones(np.array([[1, 2], [3, 4]])),
                                       [[1, 1, 2], [1, 3, 4]]))
        self.assertTrue(np.array_equal(add_bias_ones(np.array([1, 2, 3, 4])),
                                       [1, 1, 2, 3, 4]))

if __name__ == '__main__':
    unittest.main()
