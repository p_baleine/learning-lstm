import numpy as np

def sigmoid(z):
    return 1.0 / (1.0 + np.exp(-z))

def softmax(z):
    exp = np.exp(z)
    return exp / np.sum(exp, 1)[:, None]

def one_hot(x, output_size):
    eye = np.eye(output_size)
    return np.array([eye[_x] for _x in x])
